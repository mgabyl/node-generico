// *** main dependencies *** //
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var http = require('http');
var mongoose = require('mongoose');

// *** config file *** //

var config = require (path.join(__dirname,'.','config','config'));

// ***  routes *** //

var routes = require (path.join(__dirname,'.','routes','index.js'));


// *** express instance *** //
var app = express();

// *** config middleware *** //
app.use('/apidoc', express.static('apidoc'));


// *** main routes *** //
app.use('/', routes);

// *** server config *** //
var server   = http.createServer(app);
const port = config.port || 3000;

server.listen(port, function() {
    console.log('Server running in ' + port);
});

module.exports = app;
