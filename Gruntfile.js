
module.exports=function(grunt) {
    grunt.initConfig({
        create: {
            files: ['app.js',
                'test/test.js',
                'bin/.hidden',
                'config/.hidden',
                'controller/.hidden',   
                'model/.hidden',
                'public/css/.hidden',
                'public/js/.hidden',
                'public/img/.hidden',
                'public/fonts/.hidden',
                'public/js/.hidden',
                'public/stylesheets/.hidden',
                'routes/index.js',
                'services/.hidden',
                'views/layouts/main.handlebars',
                'views/home.handlebars',
                'views/404.handlebars',
                'views/500.handlebars']
        },
        apidoc: {
            myapp: {
                src: "routes/",
                dest: "apidoc/"
            }
        }
    });

    grunt.loadNpmTasks('grunt-apidoc');
    //grunt.loadNpmTasks('grunt-create');
    grunt.registerMultiTask('create', 'create dir and file', function() {
        // Merge task-specific and/or target-specific options with these defaults.
        for(var i in this.data){
            var filepath = this.data[i];
            if (!grunt.file.exists(filepath)) {
                grunt.file.write(filepath, '');
                grunt.log.writeln('File "' + filepath + '" new created.');
            } else {
                grunt.log.writeln('File "' + filepath + '" old created.');
            }
        }

    });
}
